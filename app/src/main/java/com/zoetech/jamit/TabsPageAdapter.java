package com.zoetech.jamit;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by folawiyo on 4/3/15.
 */

public class TabsPageAdapter extends FragmentPagerAdapter {


    public TabsPageAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {



        switch(position){
            case 0:
            return new PlaylistFragment();
            case 1:
            return new SongsFragment();
            case 2:
            return new ArtistsFragment();

        }


        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override

    public CharSequence getPageTitle (int position){

        switch (position){
            case 0:
                return "PLAYLIST";

            case 1:
                return "SONGS";

            case 2:
                return "ARTISTS";

        }

            return null;
    }
}

