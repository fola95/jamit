package com.zoetech.jamit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


public class PlaylistFragment extends ListFragment {



   private Button mButton;
   private ListView mListView;
   private static final String PLIST="plist";
   private static final String FILENAME="playlist.ser";
   private static final File FILE=new File(FILENAME);
   private boolean isCreated=false;
   private Context ctx;

   View view;
   private static ArrayList<String> playList;
   // String [] playList;



    public PlaylistFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState!=null){
            isCreated=savedInstanceState.getBoolean("isCreated");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_playlist, container, false);
        mButton=(Button)view.findViewById(R.id.add_playlist);
        mListView=(ListView)view.findViewById(android.R.id.list);
        if(isCreated){
            loadPlaylistData(playList);
        }else{
            playList=new ArrayList<String>();
            isCreated=true;
        }



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,playList);

        mListView.setAdapter(adapter);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new popupFragment().show(getActivity().getSupportFragmentManager(),"playlistPopup");
               storePlaylistData(playList);

            }

        });
        ctx=getActivity().getApplicationContext();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("isCreated", isCreated);
        super.onSaveInstanceState(savedInstanceState);
    }


   public static class popupFragment extends DialogFragment{
        View v;
        EditText mEditText;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
            LayoutInflater inflater = this.getActivity().getLayoutInflater();
             v=inflater.inflate(R.layout.popup, null);
            builder.setView(v)
                    .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int id) {
                            mEditText = (EditText) v.findViewById(R.id.playlist_text);
                            Log.d("dialog","doing playlist add");
                            playList.add(mEditText.getText().toString());


                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            return builder.create();
        }

    }


   public boolean  loadPlaylistData(ArrayList<String> pl){


        try
        {
            FileInputStream fileIn = ctx.openFileInput(FILENAME);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            pl= (ArrayList)in.readObject();
            in.close();
            fileIn.close();
        }catch(IOException i)
        {
            i.printStackTrace();
            return false;
        }catch(ClassNotFoundException c)
        {
            Log.d("playlist","playList data not found");
            c.printStackTrace();
            return false;
        }

       if(playList!=null)
            return true;

        return false;



    }

    public  void  storePlaylistData(ArrayList<String> pl){

        try
        {
            FileOutputStream fileOut =ctx.openFileOutput(FILENAME,Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(pl);
            out.flush();
            out.close();
            fileOut.close();
            Log.d("playlist","playlist data is saved in playlist.ser");
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }





}
