package com.zoetech.jamit;

import java.io.Serializable;

/**
 * Created by folawiyo on 4/23/15.
 */
public class PlaylistItem implements Serializable {

    private String name;

    public PlaylistItem(String name){

        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


}
